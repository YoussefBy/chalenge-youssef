package com.example.demo.models;

/**
 * @author youssef
 *
 */
public class Wallet {

	int id;
	String name;
	String type;

	public Wallet() {

	}

	public Wallet(int id, String name, String type) {
		super();
		this.id = id;
		this.name = name;
		this.type = type;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
