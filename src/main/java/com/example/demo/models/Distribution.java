package com.example.demo.models;

/**
 * @author youssef
 *
 */
public class Distribution {

	int id;
	int amount;
	String start_date;
	String end_date;
	Company company;
	User user;
	Wallet wallet;
	private static int idCounter = 1;

	public Distribution() {

	}

	public Distribution(int amount, String start_date, String end_date, Company company, User user, Wallet wallet) {
		this.id = idCounter++;
		this.amount = amount;
		this.start_date = start_date;
		this.end_date = end_date;
		this.company = company;
		this.user = user;
		this.wallet = wallet;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getStart_date() {
		return start_date;
	}

	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}

	public String getEnd_date() {
		return end_date;
	}

	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}

	public Company getCompany() {
		return company;
	}

	public void setCompany(Company company) {
		this.company = company;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Wallet getWallet() {
		return wallet;
	}

	public void setWallet(Wallet wallet) {
		this.wallet = wallet;
	}

}
