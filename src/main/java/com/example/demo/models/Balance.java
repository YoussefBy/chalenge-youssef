package com.example.demo.models;

/**
 * @author youssef
 *
 */
public class Balance {

	int wallet_id;
	int amount;

	public Balance() {

	}

	public Balance(int wallet_id, int amount) {
		this.wallet_id = wallet_id;
		this.amount = amount;
	}

	public int getWallet_id() {
		return wallet_id;
	}

	public void setWallet_id(int wallet_id) {
		this.wallet_id = wallet_id;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

}
