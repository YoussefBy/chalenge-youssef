package com.example.demo.models;

import java.util.List;

/**
 * @author youssef
 *
 */
public class User {
	int id;
	List<Balance> balanceList;

	public User() {

	}

	public User(int id, List<Balance> balanceList) {
		this.id = id;
		this.balanceList = balanceList;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public List<Balance> getBalanceList() {
		return balanceList;
	}

	public void setBalanceList(List<Balance> balanceList) {
		this.balanceList = balanceList;
	}

	

}
