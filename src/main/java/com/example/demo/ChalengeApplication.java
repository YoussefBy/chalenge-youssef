package com.example.demo;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.Date;
import java.util.Scanner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.example.demo.models.Balance;
import com.example.demo.models.Company;
import com.example.demo.models.Distribution;
import com.example.demo.models.User;
import com.example.demo.models.Wallet;
import com.example.demo.services.ManipulateFile;
import com.example.demo.services.MyService;

/**
 * @author youssef
 *
 */
@SpringBootApplication
public class ChalengeApplication {

	public static void main(String[] args) {

		SpringApplication.run(ChalengeApplication.class, args);

	}

}
