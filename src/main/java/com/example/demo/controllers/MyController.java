package com.example.demo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.models.AuthenticationRequest;
import com.example.demo.models.AuthenticationResponse;
import com.example.demo.services.IService;
import com.example.demo.services.MyUserDetailsService;
import com.example.demo.util.JwtUtil;

/**
 * @author youssef
 *
 * This class represents the RestController of the application
 */
@RestController
public class MyController {

	@Autowired
	private IService service;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private JwtUtil jwtTokenUtil;

	@Autowired
	private MyUserDetailsService userDetailsService;

	/**
	 * @param userId
	 * @param companyId
	 * @param walletId
	 * @param amount
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException
	 * @throws ParseException
	 * This method is used to call the method to distribute Gift card and meal vouchers
	 * 
	 * Date is the actual date
	 */
	@RequestMapping("/distribute/{userId}/{companyId}/{walletId}/{amount}")
	public String distribute(@PathVariable int userId, @PathVariable int companyId, @PathVariable int walletId,
			@PathVariable int amount) throws FileNotFoundException, IOException, ParseException {
		return service.distributeGiftCardMealVouchers(userId, companyId, walletId, amount);
	}

	/**
	 * @param authenticationRequest
	 * @return
	 * @throws Exception
	 * This method is used to generate the token 
	 */
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest)
			throws Exception {

		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
					authenticationRequest.getUsername(), authenticationRequest.getPassword()));
		} catch (BadCredentialsException e) {
			throw new Exception("Incorrect username or password", e);
		}

		final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

		final String jwt = jwtTokenUtil.generateToken(userDetails);

		return ResponseEntity.ok(new AuthenticationResponse(jwt));
	}

}
