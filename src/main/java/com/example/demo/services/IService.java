package com.example.demo.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;

import com.example.demo.models.User;

/**
 * @author youssef
 *
 * Interface of service
 */
public interface IService {

	public String distributeGiftCardMealVouchers(int userId, int companyId, int walletId, int amount)
			throws FileNotFoundException, IOException, ParseException;

	public int calculateBalance(User user, int walletId, int amount);
}
