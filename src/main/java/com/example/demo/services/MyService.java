package com.example.demo.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.stereotype.Service;

import com.example.demo.models.Balance;
import com.example.demo.models.Company;
import com.example.demo.models.Distribution;
import com.example.demo.models.User;
import com.example.demo.models.Wallet;

/**
 * @author youssef 
 * This class represents the service , it contains the methods used to ditribute giftcard and Meal vouchers
 * and calculate user balance
 *         
 */

@Service
public class MyService implements IService {

	ManipulateFile manipulateFile = new ManipulateFile();

	/**
	 *This method is used to ditribute giftcard and Meal vouchers
	 *Parameters are Id of user, Id of company, Id of wallet and the amount of the voucher
	 *Date is the actual date
	 */
	@Override
	public String distributeGiftCardMealVouchers(int userId, int companyId, int walletId, int amount)
			throws FileNotFoundException, IOException, ParseException {

		User user = manipulateFile.getUser(userId);
		Company company = manipulateFile.getCompany(companyId);
		Wallet wallet = manipulateFile.getWallet(walletId);

		int balanceCompany = company.getBalance();

		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date start = new Date();
		String start_date = dateFormat.format(start);
		Calendar c = Calendar.getInstance();
		c.setTime(start);
		String end_date = start_date;

		if (walletId == 1) {
			c.add(Calendar.DAY_OF_MONTH, 364);
			end_date = dateFormat.format(c.getTime());
		} else {
			c.add(Calendar.DAY_OF_MONTH, 365);

			int year = Integer.parseInt(dateFormat.format(c.getTime()).substring(0, 4));
			Date newdate = new GregorianCalendar(year, Calendar.FEBRUARY, 28).getTime();

			end_date = dateFormat.format(newdate);
		}

		company.setBalance(balanceCompany -= amount);
		calculateBalance(user, walletId, amount);
		Distribution distribution = new Distribution(amount, start_date, end_date, company, user, wallet);

		String result = manipulateFile.writeToOutput(distribution);
		return result;

	}

	/**
	 * @param user
	 * @param walletId
	 * @param amount
	 * @return 
	 * This method is used to calculate User's balance
	 */

	@Override
	public int calculateBalance(User user, int walletId, int amount) {
		List<Balance> listBalance = new ArrayList<Balance>();
		if (user.getBalanceList().isEmpty()) {
			Balance newBalance = new Balance();
			newBalance.setWallet_id(walletId);
			newBalance.setAmount(amount);
			listBalance.add(newBalance);
			user.setBalanceList(listBalance);
			return newBalance.getAmount();
		} else {
			Boolean exist = false;
			Balance balance = null;

			for (Balance b : user.getBalanceList()) {

				if (b.getWallet_id() == walletId) {
					balance = b;
					exist = true;
				}

			}
			if (exist) {
				int balanceUser = balance.getAmount();
				balance.setAmount(balanceUser += amount);
				return balance.getAmount();
			} else {
				balance = new Balance();
				balance.setWallet_id(walletId);
				balance.setAmount(amount);

				user.getBalanceList().add(balance);
				return balance.getAmount();
			}

		}

	}


}
