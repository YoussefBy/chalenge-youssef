package com.example.demo.services;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import com.example.demo.models.Balance;
import com.example.demo.models.Company;
import com.example.demo.models.Distribution;
import com.example.demo.models.User;
import com.example.demo.models.Wallet;

/**
 * @author youssef
 *
 * This class is used to manipulate data from and to the file data.json
 */

public class ManipulateFile {

	/**
	 * The path to the file
	 */
	String filePath = "src/main/resources/data.json";

	/**
	 * @param idComp
	 * @return This method is used to get a company by it's ID
	 */
	public Company getCompany(int idComp) {
		Company company = null;

		JSONParser parser = new JSONParser();

		try {
			Object obj = parser.parse(new FileReader(filePath));
			JSONObject jsonObject = (JSONObject) obj;

			JSONArray companies = (JSONArray) jsonObject.get("companies");

			Iterator<JSONObject> iteratorC = companies.iterator();
			while (iteratorC.hasNext()) {

				JSONObject co = iteratorC.next();
				if (Integer.parseInt(co.get("id").toString()) == idComp) {
					company = new Company();
					company.setId(Integer.parseInt(co.get("id").toString()));
					company.setName((String) co.get("name"));
					company.setBalance(Integer.parseInt(co.get("balance").toString()));
				}

			}
			if (company == null) {
				System.out.println("Please enter a valid ID");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return company;

	}

	/**
	 * @param idUser
	 * @return This method is used to get a user by it's ID
	 */
	public User getUser(int idUser) {
		User user = null;
		Balance balance = null;
		List<Balance> listBalance = new ArrayList<Balance>();

		JSONParser parser = new JSONParser();

		try {
			Object obj = parser.parse(new FileReader(filePath));

			JSONObject jsonObject = (JSONObject) obj;

			JSONArray users = (JSONArray) jsonObject.get("users");

			Iterator<JSONObject> iteratorU = users.iterator();
			while (iteratorU.hasNext()) {

				JSONObject u = iteratorU.next();
				if (Integer.parseInt(u.get("id").toString()) == idUser) {
					user = new User();

					user.setId(Integer.parseInt(u.get("id").toString()));

					JSONArray balances = (JSONArray) u.get("balance");
					Iterator<JSONObject> iteratorB = balances.iterator();
					while (iteratorB.hasNext()) {
						JSONObject b = iteratorB.next();

						balance = new Balance();
						balance.setWallet_id(Integer.parseInt(b.get("wallet_id").toString()));
						balance.setAmount(Integer.parseInt(b.get("amount").toString()));
						listBalance.add(balance);
					}

					user.setBalanceList(listBalance);
				}

			}
			if (user == null) {
				System.out.println("Please enter a valid ID ");

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return user;

	}

	/**
	 * @param idWallet
	 * @return
	 * 
	 *  This method is used to get a wallet by it's ID
	 */
	public Wallet getWallet(int idWallet) {
		Wallet wallet = null;

		JSONParser parser = new JSONParser();

		try {
			Object obj = parser.parse(new FileReader(filePath));
			JSONObject jsonObject = (JSONObject) obj;

			JSONArray wallets = (JSONArray) jsonObject.get("wallets");

			Iterator<JSONObject> iteratorW = wallets.iterator();
			while (iteratorW.hasNext()) {

				JSONObject wa = iteratorW.next();
				if (Integer.parseInt(wa.get("id").toString()) == idWallet) {
					wallet = new Wallet();
					wallet.setId(Integer.parseInt(wa.get("id").toString()));
					wallet.setName((String) wa.get("name"));
					wallet.setType((String) wa.get("type"));
				}

			}
			if (wallet == null) {
				System.out.println("Please enter a valid ID");
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return wallet;

	}

	/**
	 * @param distribution
	 * @return
	 * @throws FileNotFoundException
	 * @throws IOException          
	 *  This method is used to to append the new data in  an output file
	 *                       
	 */
	public String writeToOutput(Distribution distribution) throws FileNotFoundException, IOException {
		JSONObject finalobj = new JSONObject();
		JSONParser parser = new JSONParser();

		int i = 0;
		try {
			Object obj = parser.parse(new FileReader(filePath));

			JSONObject jsonObject = (JSONObject) obj;

			JSONArray wallets = (JSONArray) jsonObject.get("wallets");
			JSONArray companies = (JSONArray) jsonObject.get("companies");
			JSONArray users = (JSONArray) jsonObject.get("users");
			JSONArray distributions = (JSONArray) jsonObject.get("distributions");

			JSONObject newDistribution = new JSONObject();
			newDistribution.put("id", distribution.getId());
			newDistribution.put("amount", distribution.getAmount());
			newDistribution.put("wallet_id", distribution.getWallet().getId());
			newDistribution.put("start_date", distribution.getStart_date());
			newDistribution.put("end_date", distribution.getEnd_date());
			newDistribution.put("company_id", distribution.getCompany().getId());
			newDistribution.put("user_id", distribution.getUser().getId());

			distributions.add(newDistribution);

			Iterator<JSONObject> iteratorC = companies.iterator();
			while (iteratorC.hasNext()) {

				JSONObject c = iteratorC.next();
				if (Integer.parseInt(c.get("id").toString()) == distribution.getCompany().getId()) {
					c.remove("balance");
					c.put("balance", distribution.getCompany().getBalance());
				}

				Iterator<JSONObject> iteratorU = users.iterator();

				while (iteratorU.hasNext()) {

					JSONObject u = iteratorU.next();
					if (Integer.parseInt(u.get("id").toString()) == distribution.getUser().getId()) {

						JSONArray balances = (JSONArray) u.get("balance");

						Iterator<JSONObject> iteratorB = balances.iterator();
						if (!iteratorB.hasNext()) {

							JSONObject b = new JSONObject();
							b.put("wallet_id", distribution.getUser().getBalanceList().get(0).getWallet_id());
							b.put("amount", distribution.getUser().getBalanceList().get(0).getAmount());
							balances.add(b);
						} else {

							JSONObject newB = null;
							while (iteratorB.hasNext()) {

								JSONObject objB = iteratorB.next();
								Balance balance = distribution.getUser().getBalanceList().get(i);
								if (Integer.parseInt(objB.get("wallet_id").toString()) == balance.getWallet_id()) {
									objB.remove("amount");
									objB.put("amount", distribution.getUser().getBalanceList().get(i).getAmount());

								} else {
									newB = new JSONObject();
									newB.put("wallet_id", balance.getWallet_id());
									newB.put("amount", balance.getAmount());

								}
								i++;

							}
							if (newB != null) {
								balances.add(newB);
							}

						}

					}
				}

				finalobj.put("wallets", wallets);
				finalobj.put("companies", companies);
				finalobj.put("users", users);
				finalobj.put("distributions", distributions);

				try (FileWriter file = new FileWriter(filePath, false)) {

					file.write(finalobj.toJSONString());
					file.flush();

				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		} catch (Exception e) {
			// e.printStackTrace();
		}
		return finalobj.toJSONString();

	}

}
