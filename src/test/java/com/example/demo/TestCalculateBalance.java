package com.example.demo;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONArray;
import org.junit.jupiter.api.Test;

import com.example.demo.models.Balance;
import com.example.demo.models.User;
import com.example.demo.services.MyService;

class TestCalculateBalance {

	@Test
	void test() {
		List<Balance> list=new ArrayList<Balance>();
		Balance balance =new Balance(1,100);
		list.add(balance);
		MyService service=new MyService();
		
		User user=new User(0, list);
		int output=service.calculateBalance(user,1, 50);
		
		assertEquals(150, output);
	}
	

}
